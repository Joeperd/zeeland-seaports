package nl.zeelandseaports.aishub;

import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import dk.dma.ais.message.AisMessage;
import dk.dma.ais.message.AisMessage4;
import dk.dma.ais.message.AisPositionMessage;
import dk.dma.db.cassandra.CassandraConnection;
import dk.dma.db.cassandra.PasswordProtectedCassandraConnection;

import java.security.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static dk.dma.commons.util.EnvironmentUtil.env;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Created by leonbdv on 16-01-17.
 */
public class AISWriter {

    private final static String ENV_KEY_AISSTORE_USER = "AISSTORE_USER";
    private final static String ENV_KEY_AISSTORE_PASS = "AISSTORE_PASS";

    boolean secureConnection = false;
    String keyspaceName = "aisdata";
    List<String> seeds = Arrays.asList("localhost");

    private CassandraConnection connection;


    public AISWriter() {
       connection = connect(seeds, keyspaceName, secureConnection);
       connection.startAsync();
    }

    /**
     * Create a new (non-started) connection to AisStore.
     *
     * @return A new connection to AisStore. Null if no seeds or database name are provided.
     */
    static CassandraConnection connect(List<String> seeds, String keyspace, boolean secure) {
        CassandraConnection con;
        if (seeds != null && seeds.size() > 0 && !isBlank(keyspace)) {
            if (secure) {
                System.out.println("Creating secure Cassandra connection.");
                con = PasswordProtectedCassandraConnection.create(env(ENV_KEY_AISSTORE_USER), env(ENV_KEY_AISSTORE_PASS), keyspace, seeds);
            } else {
                System.out.println("Creating unsecure Cassandra connection.");
                con = CassandraConnection.create(keyspace, seeds);
            }
        } else {
            System.out.println("No seeds or keyspace for AisStore. No connection established.");
            con = null;
        }
        return con;
    }

    public void saveData(AisPositionMessage aisMessage){
            long timestamp = new Date().getTime();
            Insert i = QueryBuilder.insertInto("aismessages");
            i.value("mmsi",aisMessage.getUserId());
            i.value("navStatus",aisMessage.getNavStatus());
            i.value("rot",aisMessage.getRot());
            i.value("sog",aisMessage.getSog());
            i.value("posAcc",aisMessage.getPosAcc());
            if(aisMessage.getValidPosition() != null){
                i.value("longitude",aisMessage.getValidPosition().getLongitude());
                i.value("latitude",aisMessage.getValidPosition().getLatitude());
            }
            i.value("cog",aisMessage.getCog());
            i.value("trueheading",aisMessage.getTrueHeading());
            i.value("utcSec",aisMessage.getUtcSec());
            i.value("specialManIndicator",aisMessage.getSpecialManIndicator());
            i.value("raim",aisMessage.getRaim());
            i.value("syncState",aisMessage.getSyncState());
            i.value("timestamp",timestamp);

            connection.getSession().execute(i);
    }
}
