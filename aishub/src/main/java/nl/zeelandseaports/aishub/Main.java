package nl.zeelandseaports.aishub;

import dk.dma.ais.message.AisMessage;
import dk.dma.ais.message.AisPositionMessage;
import dk.dma.ais.reader.AisReader;
import dk.dma.ais.reader.AisReaders;

import java.util.function.Consumer;

public class Main {

    public static void main(String[] args) throws Exception {
        new Main();
    }

    public Main(){
        AisReader reader = AisReaders.createReader("localhost", 2017);

        final AISWriter writer = new AISWriter();

        reader.registerHandler(new Consumer<AisMessage>() {
            @Override
            public void accept(AisMessage aisMessage) {
                if(aisMessage instanceof AisPositionMessage) {
                    System.out.println("aismessage instance of position message");
                    writer.saveData((AisPositionMessage) aisMessage);
                }
                System.out.println("message id: " + aisMessage.getMsgId());
            }
        });
        reader.start();

        try {
            reader.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    }
