package dk.tbsalling.aismessages.demo;

/**
 * Created by Pim on 11-1-2017.
 */
public class split {
    public static void main(String[] args) {
        String sampleString = "PositionReportClassAScheduled{messageType=PositionReportClassAScheduled} PositionReport{navigationStatus=UnderwayUsingEngine, rateOfTurn=0, speedOverGround=0.3, positionAccuracy=false, latitude=37.839558, longitude=-122.45115, courseOverGround=24.3, trueHeading=78, second=11, specialManeuverIndicator=NotAvailable, raimFlag=false} AISMessage{nmeaMessages=[NMEAMessage{rawMessage='!AIVDM,1,1,,B,15N0;wP003G?Ma4EafMPtjLF086=,0*58'}], metadata=Metadata{source='DEMOSRC1', received=2017-01-11T12:40:12.245Z}, repeatIndicator=0, sourceMmsi=MMSI [mmsi=367004670]}";
        String[] nmea = sampleString.split(",");
        System.out.println("the string: " + nmea.length);
        for (String ais : nmea) {
            System.out.println(ais);
        }
    }
}
